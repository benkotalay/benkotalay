import { Http } from '@angular/http';
import 'rxjs/Rx';
import { Response } from '@angular/http/src/static_response';
import { Injectable } from '@angular/core';
import { ShowMovie } from '../data/showMovie.interface';

@Injectable()
export class DatabaseService{
    public showMovies: ShowMovie[] = [];
    constructor(public http: Http){}
    // baseUrl:string = "http://localhost/";
    baseUrl:string = "https://hendrowijaya.com/";
    getShowMovies(){
        var data = this.requestData(this.baseUrl+"benkotlay/phpShowMovies.php");
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        })
        // return this.showMovies;
    }

    getBuyTickets(movieId: any, watchingDate: any, showNumber: any, studio: any){
        var data = this.requestDataSeat(this.baseUrl+"benkotlay/phpGetDataSeat.php?movieid="+movieId+"&date="+watchingDate+"&time="+showNumber+"&studio="+studio);
        return new Promise(resolve => {
            data.then((res) => {
                console.log(res);
                resolve(res);
            });
        });
    }

    getLastSeatToPay(movieId: any, watchingDate: any, showNumber: any, studio: any){
        var data = this.requestDataSeat(this.baseUrl+"benkotlay/getLastSeat.php?movieid="+movieId+"&date="+watchingDate+"&time="+showNumber+"&studio="+studio);
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        });
    }

    insertBuyTickets(movieId: any, watchingDate: any, showNumber: any, seatId: any, price: any, uid: any){
        var data = this.requestDataInsert(this.baseUrl+"benkotlay/phpInsertSeat.php?movieid="+movieId+"&date="+watchingDate+"&time="+showNumber+"&seatid="+seatId+"&price="+price+"&uid="+uid+"&action=ticket");
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        });
    }

    buyVoucher(amount: any, ccNumber: any, uid: any){
        console.log(uid);
        console.log(amount);
        var data = this.requestDataString(this.baseUrl+"benkotlay/phpVoucher.php?action=buy&amount="+amount+"&ccNumber="+ccNumber+"&uid="+uid);
        return new Promise(resolve => {
            data.then((res) => {
                //console.log(res);
                resolve(res);
            });
        });
    }

    redeemVoucher(vid: any){
        var data = this.requestDataString(this.baseUrl+"benkotlay/phpVoucher.php?action=redeem&vid="+vid);
        return new Promise(resolve => {
            data.then((res) => {
                //console.log(res);
                resolve(res);
            });
        });
    }

    historyData(action: any, uid: any){
        var data = this.requestDataString(this.baseUrl+"benkotlay/phpHistory.php?action="+action+"&uid="+uid);
        return new Promise(resolve => {
            data.then((res) => {
                //console.log(res);
                resolve(res);
            });
        });
    }

    requestData(url: string){
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
            //   console.log(data.showMovies);
              this.showMovies = data.showMovies;
              resolve(data.showMovies);
            });
        });
        // this.http.get(url)
        // .map(res => res.json())
        // .subscribe(data => {
        // //   console.log(data.showMovies);
        //     this.showMovies = data.showMovies;
            
        // });
    }

    requestDataSeat(url: string){
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
            //   console.log(data.showMovies);
              resolve(data);
            });
        });
    }

    requestDataString(url: string){
        console.log(url);
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
                resolve(data);
            });
        });
    }

    requestDataInsert(url: string){
        console.log(url);
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
              console.log(data);
              resolve(data);
            });
        });
    }
}
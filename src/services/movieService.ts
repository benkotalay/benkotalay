import 'rxjs/Rx';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class MovieService{
    str = 'https://api.themoviedb.org/3/movie/118406?api_key=4f5b562b3e6b26237b4924dfa012e260&language=en-US'
    apiKey = '4f5b562b3e6b26237b4924dfa012e260';
    hasil: any;
    showMovies: any;
    details:any;
    constructor(public http:Http){

    }

    getAllShowMovies(arrMovie: any){
        for(let i=0; i<arrMovie.length; i++){
            this.getDetails(arrMovie[i].movie_id).then((res) => {
                this.getVideos(arrMovie[i].movie_id).then(result => {
                    arrMovie[i]['title'] = res['title'];
                    arrMovie[i]['poster_path'] = res['poster_path'];
                    arrMovie[i]['overview'] = res['overview'];
                    arrMovie[i]['genres'] = res['genres'];
                    arrMovie[i]['duration'] = res['runtime'];
                    arrMovie[i]['vote_average'] = res['vote_average'];
                    arrMovie[i]['release_date'] = res['release_date'];
                    arrMovie[i]['runtime'] = res['runtime'];
                    arrMovie[i]['production_companies'] = res['production_companies'];
                    arrMovie[i]['youtube'] = result['results']['0']['key'];
                });
                
            });
        }
        this.showMovies = arrMovie;
        return new Promise(resolve => {
            resolve(this.showMovies);
        })
        // return this.showMovies;
    }

    getDataSearch(title: string){
        var dataMovie = [];
        this.getSearchMovie(title).then((res) => {
            var size = res['results'].length;
            for(let i = 0; i<size;i++){
                dataMovie.push(res['results'][i]);
            }
        });
        return new Promise(resolve => {
            resolve(dataMovie);
        })
    }

    getComingSoon(){
        var dataMovie = [];
        this.getUpcoming("1").then((res) => {
            var size = res['results'].length;
            for(let i = 0; i<size;i++){
                dataMovie.push(res['results'][i]);
            }
        });
        return new Promise(resolve => {
            resolve(dataMovie);
        })
    }

    getShowMovies(){
        return this.showMovies;
    }

    //MOVIE
    getNowPlaying(page: string){
        this.requestData('https://api.themoviedb.org/3/movie/now_playing?api_key='+ this.apiKey +'&language=en-US&page=' + page);
        return this.hasil;
    }

    getPopular(page: string){
        this.requestData('https://api.themoviedb.org/3/movie/popular?api_key='+ this.apiKey +'&language=en-US&page=' + page);
        return this.hasil;
    }

    getTopRated(page: string){
        this.requestData('https://api.themoviedb.org/3/movie/top_rated?api_key='+ this.apiKey +'&language=en-US&page=' + page);
        return this.hasil;
    }

    getUpcoming(page: string){
        var data = this.requestData('https://api.themoviedb.org/3/movie/upcoming?api_key='+ this.apiKey +'&language=en-US&page=' + page);
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        })
    }

    getDetails(movieId){
        var data = this.requestData('https://api.themoviedb.org/3/movie/'+ movieId +'?api_key='+ this.apiKey +'&language=en-US');
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        })
    }

    getSearchMovie(title: string){
        var url = "https://api.themoviedb.org/3/search/movie?api_key=" + this.apiKey + "&language=en-US&query=" + title;
        var data = this.requestData(url);
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        })
    }

    getVideos(movieId: string){
        var data = this.requestData('https://api.themoviedb.org/3/movie/'+ movieId +'/videos?api_key='+ this.apiKey +'&language=en-US');
        // return this.hasil;
        return new Promise(resolve => {
            data.then((res) => {
                resolve(res);
            });
        })
    }

    getReviews(movieId: string, page: string){
        this.requestData('https://api.themoviedb.org/3/movie/'+ movieId +'/reviews?api_key='+ this.apiKey +'&language=en-US&page=' + page);
        return this.hasil;
    }


    //GENRE
    getAllGenre(){
        this.requestData('https://api.themoviedb.org/3/genre/movie/list?api_key='+ this.apiKey +'&language=en-US');
        return this.hasil;
    }

    getMovieFromGenre(genreId: string){
        this.requestData('https://api.themoviedb.org/3/genre/'+ genreId +'/movies?api_key='+ this.apiKey +'&language=en-US&include_adult=false&sort_by=created_at.asc');
        return this.hasil;
    }

    requestData(url: string){
        return new Promise(resolve => {
            this.http.get(url)
            .map(res => res.json())
            .subscribe(data => {
                // console.log(data);
                this.hasil=data;
                resolve(this.hasil);
            });
        });
    }
}
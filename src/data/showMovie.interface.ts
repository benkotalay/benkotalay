export interface ShowMovie{
    movie_id: string,
    studio_id: string,
    startda: string,
    endda: string
}
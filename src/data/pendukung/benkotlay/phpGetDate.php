<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header("Access-Control-Allow-Headers: X-Requested-With");
	date_default_timezone_set("Asia/Jakarta");
	$date = ['date'=>date("Y-m-d#h:i:s a")];
	print(json_encode($date));
?>
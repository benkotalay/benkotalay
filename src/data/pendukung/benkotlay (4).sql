-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2017 at 09:14 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `benkotlay`
--

-- --------------------------------------------------------

--
-- Table structure for table `buytickets`
--

CREATE TABLE `buytickets` (
  `ticket_id` varchar(50) NOT NULL,
  `uid` varchar(50) NOT NULL,
  `device_id` varchar(50) NOT NULL,
  `movie_id` varchar(10) NOT NULL,
  `seat_id` varchar(10) NOT NULL,
  `price` int(11) NOT NULL,
  `watching_date` date NOT NULL,
  `show_number` int(11) NOT NULL,
  `sold_time` datetime NOT NULL,
  `credit_card_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buytickets`
--

INSERT INTO `buytickets` (`ticket_id`, `uid`, `device_id`, `movie_id`, `seat_id`, `price`, `watching_date`, `show_number`, `sold_time`, `credit_card_number`) VALUES
('17120800001', '', 'tgr01m01', '203113', 'tgr0101a1', 50000, '2017-12-08', 1, '2017-12-03 10:00:00', '5429741658741537');

-- --------------------------------------------------------

--
-- Table structure for table `machines`
--

CREATE TABLE `machines` (
  `machine_id` varchar(50) NOT NULL,
  `machine_name` varchar(150) NOT NULL,
  `theater_id` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `machines`
--

INSERT INTO `machines` (`machine_id`, `machine_name`, `theater_id`, `password`) VALUES
('tgr01m01', 'Machine 1 Cinema Benkotlay Tangerang', 'tgr01', 'adminm1'),
('tgr01m02', 'Machine 2 Cinema Benkotlay Tangerang', 'tgr01', 'adminm2');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `seat_id` varchar(20) NOT NULL,
  `col` varchar(5) NOT NULL,
  `row` varchar(5) NOT NULL,
  `studio_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `showmovies`
--

CREATE TABLE `showmovies` (
  `movie_id` varchar(10) NOT NULL,
  `studio_id` varchar(10) NOT NULL,
  `startda` varchar(50) NOT NULL,
  `endda` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `showmovies`
--

INSERT INTO `showmovies` (`movie_id`, `studio_id`, `startda`, `endda`) VALUES
('118406', 'tgr0102', '2017-11-30', '2017-12-31'),
('203113', 'tgr0101', '2017-11-01', '2017-12-31'),
('22421', 'tgr0103', '2017-11-01', '2017-12-31'),
('392044', 'tgr0104', '2018-01-01', '2018-01-08'),
('467012', 'tgr0104', '2017-11-01', '2017-12-31'),
('49526', 'tgr0102', '2017-11-01', '2017-11-29'),
('64004', 'tgr0105', '2017-11-01', '2017-12-31');

-- --------------------------------------------------------

--
-- Table structure for table `studios`
--

CREATE TABLE `studios` (
  `studio_id` varchar(10) NOT NULL,
  `theater_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `studios`
--

INSERT INTO `studios` (`studio_id`, `theater_id`) VALUES
('tgr0101', 'tgr01'),
('tgr0102', 'tgr01'),
('tgr0103', 'tgr01'),
('tgr0104', 'tgr01'),
('tgr0105', 'tgr01');

-- --------------------------------------------------------

--
-- Table structure for table `theaters`
--

CREATE TABLE `theaters` (
  `theater_id` varchar(10) NOT NULL,
  `theater_name` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theaters`
--

INSERT INTO `theaters` (`theater_id`, `theater_name`, `address`, `city`, `phone`) VALUES
('tgr01', 'Cinema Benkotlay Tangerang', 'Jln Benteng Kota Alay 1, Tangerang, Banten', 'Tangerang', '02155777777');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buytickets`
--
ALTER TABLE `buytickets`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `machines`
--
ALTER TABLE `machines`
  ADD PRIMARY KEY (`machine_id`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`seat_id`);

--
-- Indexes for table `showmovies`
--
ALTER TABLE `showmovies`
  ADD PRIMARY KEY (`movie_id`,`studio_id`);

--
-- Indexes for table `studios`
--
ALTER TABLE `studios`
  ADD PRIMARY KEY (`studio_id`);

--
-- Indexes for table `theaters`
--
ALTER TABLE `theaters`
  ADD PRIMARY KEY (`theater_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MovieService } from '../services/movieService';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { PlayingPage } from '../pages/playing/playing';
import { ComingSoonPage } from '../pages/coming-soon/coming-soon';
import { DatabaseService } from '../services/databaseService';
import { SignupPage } from '../pages/signup/signup';
import { AuthService } from '../services/authService';
import { AboutusPage } from '../pages/aboutus/aboutus';
import {PopovermenuPage} from '../pages/popovermenu/popovermenu';
import {SeatingPage} from '../pages/seating/seating';
import {MovieDetailPage} from '../pages/movie-detail/movie-detail';
import { PaymentPage } from '../pages/payment/payment';
import { TopUpPage } from '../pages/top-up/top-up';
import { SearchPage } from '../pages/search/search';
import { MovieProvider } from '../providers/movie-provider';
import { ProfilePage } from '../pages/profile/profile';
// import { EmailComposer } from '@ionic-native/email-composer';
import { TicketPage } from '../pages/ticket/ticket';
import { AgmCoreModule } from '@agm/core';
import { HistoryPage } from '../pages/history/history';
import { SideHistoryPage } from '../pages/side-history/side-history';
import { SynopsisPage } from '../pages/synopsis/synopsis';
import { TrailerPage } from '../pages/trailer/trailer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    PlayingPage,
    ComingSoonPage,
    SignupPage,
    AboutusPage,
    PopovermenuPage,
    SeatingPage,
    MovieDetailPage,
    PaymentPage,
    TopUpPage,
    SearchPage,
    ProfilePage,
    TicketPage,
    HistoryPage,
    SideHistoryPage,
    SynopsisPage,
    TrailerPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCAIF0s2OQo9AqCefiTJEIFkA-0QRFe4hs'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    TabsPage,
    PlayingPage,
    ComingSoonPage,
    SignupPage,
    AboutusPage,
    PopovermenuPage,
    SeatingPage,
    MovieDetailPage,
    PaymentPage,
    TopUpPage,
    SearchPage,
    ProfilePage,
    TicketPage,
    HistoryPage,
    SideHistoryPage,
    SynopsisPage,
    TrailerPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseService,
    AuthService,
    MovieService,
    MovieProvider,
    // EmailComposer
  ]
})
export class AppModule {}

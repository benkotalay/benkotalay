import { Component, ViewChild } from '@angular/core';
import { Platform, AlertController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { DatabaseService } from '../services/databaseService';
import firebase from "firebase";
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { SignupPage } from '../pages/signup/signup';
import { PlayingPage } from '../pages/playing/playing';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../services/authService';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { MovieService } from '../services/movieService';
import { PaymentPage } from '../pages/payment/payment';
import { TopUpPage } from '../pages/top-up/top-up';
import { SearchPage } from '../pages/search/search';
import { ProfilePage } from '../pages/profile/profile';
import { SideHistoryPage } from '../pages/side-history/side-history';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  active = 0;
  rootPage:any = TabsPage;
  loginPage = LoginPage;
  aboutusPage = AboutusPage;
  paymentPage = PaymentPage;
  topupPage = TopUpPage;
  profilePage = ProfilePage;
  sideHistoryPage = SideHistoryPage;
  showMovies:any;

  @ViewChild('sideMenuContent') nav: NavController;
  constructor(platform: Platform, statusBar: StatusBar,private modalCtrl: ModalController, private alertCtrl: AlertController, private authService: AuthService, private loadingCtrl: LoadingController, splashScreen: SplashScreen, public dbSvc: DatabaseService, public menuCtrl: MenuController, public authSvc: AuthService, public movieService: MovieService) {
    firebase.initializeApp({
      apiKey: "AIzaSyCP7QbYEOlmMpn0RuUd28q_HM018a9opvg",
      authDomain: "cinema-benkotlay.firebaseapp.com",
      databaseURL: "https://cinema-benkotlay.firebaseio.com",
      projectId: "cinema-benkotlay",
      storageBucket: "cinema-benkotlay.appspot.com",
      messagingSenderId: "152288932132"
    });

    firebase.auth().onAuthStateChanged(user => {
      if(user){
        this.active = 1;
        this.authService.setFlagUser(this.active);

        console.log('ini nanti pindahin ke playingPage');
        let loading = this.loadingCtrl.create({
          spinner: 'bubbles',
          content: 'Loading Please Wait...'
        });
      
        loading.present();
      
        setTimeout(() => {
          this.nav.setRoot(TabsPage);
          loading.dismiss();
        }, 2000);
      }
      else{
        this.active = 0;
        this.authService.setFlagUser(this.active);
        this.nav.setRoot(TabsPage);
      }
    })

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // this.dbSvc.getShowMovies();
    
  }

  onLoad(page: any){
    if(page == "aboutusPage"){
      this.nav.push(this.aboutusPage);
    }else if(page == "topupPage"){
      this.nav.push(this.topupPage);
    }else if(page == "profilePage"){
      this.nav.push(this.profilePage);
    }else if(page == "sideHistoryPage"){
      this.nav.push(this.sideHistoryPage);
    }else{
      this.nav.setRoot(page);
    }
    this.menuCtrl.close();
  }

  signOut(){
    this.menuCtrl.close();
    this.authSvc.logout();
    this.nav.setRoot(TabsPage);
  }

  loginPrompt(){
    this.menuCtrl.close();
    const signIn = this.alertCtrl.create({
      title: 'Sign In',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Password'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Sign In',
          handler: data=> {
            var email = data.email;
            var password = data.password;
            this.authService.signin(email, password).then(param=>{
              this.menuCtrl.close();
            }).catch(
              error => {
                const alert = this.alertCtrl.create({
                  title: 'Error',
                  message: error.message,
                  buttons: [{
                    text: 'Ok',
                    role: 'cancel',
                  }
                  ]
                });
                alert.present();
              }
            );
          }
        }
      ]
    });
    signIn.present();
  }

  register(){
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
    modal.onDidDismiss(param =>{
      this.menuCtrl.close();
    });
  }

  changePass(){
    this.menuCtrl.close();
    const resetPass = this.alertCtrl.create({
      title: 'Reset Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Reset',
          handler: data => {
            var email = data.email;
            var auth = firebase.auth();
            auth.sendPasswordResetEmail(email).then(function(){
              const alert = this.alertCtrl.create({
                title: 'Information',
                message: "Please check your email to get new password.",
                buttons: [{
                  text: 'Ok',
                  role: 'cancel',
                }
                ]
              });
              alert.present();
            }).catch(function(err){
              const alert = this.alertCtrl.create({
                title: 'Error',
                message: "Can't reset password, try again.",
                buttons: [{
                  text: 'Ok',
                  role: 'cancel',
                }
                ]
              });
              alert.present();
            });
          }
        }
      ]
    });
    resetPass.present();
  }
}


import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SynopsisPage } from './synopsis';

@NgModule({
  declarations: [
    SynopsisPage,
  ],
  imports: [
    IonicPageModule.forChild(SynopsisPage),
  ],
})
export class SynopsisPageModule {}

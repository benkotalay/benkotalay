import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SynopsisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-synopsis',
  templateUrl: 'synopsis.html',
})
export class SynopsisPage {
  movie:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.movie = this.navParams.get('movie');
    console.log(this.movie);
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad SynopsisPage');
    
  }

}

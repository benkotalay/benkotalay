import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { DatabaseService } from '../../services/databaseService';
import * as $ from 'jquery';
import { PaymentPage } from '../payment/payment';

/**
 * Generated class for the SeatingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-seating',
  templateUrl: 'seating.html',
})
export class SeatingPage implements OnInit{
  formSeat: FormGroup;
  sub: any;
  choosenSeat: any = [];
  flagChoose = 0;
  movieId: any;
  scheduleDate: any;
  scheduleTime: any;
  studioId: any;
  hari: any;
  film: any;
  resultParam: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public dbSvc: DatabaseService, public alertCtrl: AlertController) {
    this.movieId = this.navParams.get('paramMovie');
    this.scheduleDate = this.navParams.get('paramDate');
    this.scheduleTime = this.navParams.get('paramTime');
    this.studioId = this.navParams.get('paramStudio');
    this.hari = this.navParams.get('paramHari');
    this.film = this.navParams.get('paramFilm');
    // this.sub = Observable.interval(1000).subscribe((val) => {
    //   // console.log('called');
    //   this.dbSvc.getBuyTickets(this.movieId,this.scheduleDate,this.scheduleTime,this.studioId).then((res) => {
    //     // console.log(res);
    //     if(res['success'] == 1){
    //       for(let i = 0;i<res['buytickets'].length;i++){
    //         var id = res['buytickets'][i]['seat_id'];
    //         id = id.substring(7);
    //         // console.log(id);
    //         $('#'+id).attr('disabled',true);
    //         // $('#'+id).val(false);
    //       }
    //     }
    //   });
    // });
  }

  testDoang(){
    console.log("test");
    $("#seat1A").attr('disabled',true);
    // $("#test").text('hege');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SeatingPage');
  }

  ionViewWillLeave(){
    this.sub.unsubscribe();
  }
  
  ionViewWillEnter(){
    this.sub = Observable.interval(1000).subscribe((val) => {
      // console.log('called');
      this.dbSvc.getBuyTickets(this.movieId,this.scheduleDate,this.scheduleTime,this.studioId).then((res) => {
        // console.log(res);
        if(res['success'] == 1){
          for(let i = 0;i<res['buytickets'].length;i++){
            var id = res['buytickets'][i]['seat_id'];
            id = id.substring(7);
            // console.log(id);
            $('#'+id).attr('disabled',true);
            // $('#'+id).val(false);
          }
        }
      });
    });
  }

  ngOnInit(){
    this.initializeForm();
  }

  onSubmit(){
    console.log(this.formSeat.value);
    this.choosenSeat = [];
    // if(this.flagChoose == 0){
      for(let i = 1;i<=8;i++){
        for(let j = 1;j<=8;j++){
          var huruf;
          if(j == 1){
            huruf = 'a';
          }else if(j == 2){
            huruf = 'b';
          }else if(j == 3){
            huruf = 'c';
          }else if(j == 4){
            huruf = 'd';
          }else if(j == 5){
            huruf = 'e';
          }else if(j == 6){
            huruf = 'f';
          }
          else if(j == 7){
            huruf = 'g';
          }
          else if(j == 8){
            huruf = 'h';
          }

          if(this.formSeat.value['seat'+i+huruf] == true && $('#'+huruf+i).prop('disabled') == false){
            this.choosenSeat.push(huruf+i);
          }
        }
      }
      // if(this.formSeat.value.seat1a == true && $('#a1').prop('disabled') == false){
      //   this.choosenSeat.push('a1');
      // }
      // if(this.formSeat.value.seat1b == true && $('#b1').prop('disabled') == false){
      //   this.choosenSeat.push('b1');
      // }
      // if(this.formSeat.value.seat1c == true && $('#c1').prop('disabled') == false){
      //   this.choosenSeat.push('c1');
      // }
      // if(this.formSeat.value.seat1d == true && $('#d1').prop('disabled') == false){
      //   this.choosenSeat.push('d1');
      // }
      // if(this.formSeat.value.seat1e == true && $('#e1').prop('disabled') == false){
      //   this.choosenSeat.push('e1');
      // }
      // if(this.formSeat.value.seat1f == true && $('#f1').prop('disabled') == false){
      //   this.choosenSeat.push('f1');
      // }
  
      // if(this.formSeat.value.seat2a == true && $('#a2').prop('disabled') == false){
      //   this.choosenSeat.push('a2');
      // }
      // if(this.formSeat.value.seat2b == true && $('#b2').prop('disabled') == false){
      //   this.choosenSeat.push('b2');
      // }
      // if(this.formSeat.value.seat2c == true && $('#c2').prop('disabled') == false){
      //   this.choosenSeat.push('c2');
      // }
      // if(this.formSeat.value.seat2d == true && $('#d2').prop('disabled') == false){
      //   this.choosenSeat.push('d2');
      // }
      // if(this.formSeat.value.seat2e == true && $('#e2').prop('disabled') == false){
      //   this.choosenSeat.push('e2');
      // }
      // if(this.formSeat.value.seat2f == true && $('#f2').prop('disabled') == false){
      //   this.choosenSeat.push('f2');
      // }
  
      // if(this.formSeat.value.seat3a == true && $('#a3').prop('disabled') == false){
      //   this.choosenSeat.push('a3');
      // }
      // if(this.formSeat.value.seat3b == true && $('#b3').prop('disabled') == false){
      //   this.choosenSeat.push('b3');
      // }
      // if(this.formSeat.value.seat3c == true && $('#c3').prop('disabled') == false){
      //   this.choosenSeat.push('c3');
      // }
      // if(this.formSeat.value.seat3d == true && $('#d3').prop('disabled') == false){
      //   this.choosenSeat.push('d3');
      // }
      // if(this.formSeat.value.seat3e == true && $('#e3').prop('disabled') == false){
      //   this.choosenSeat.push('e3');
      // }
      // if(this.formSeat.value.seat3f == true && $('#f3').prop('disabled') == false){
      //   this.choosenSeat.push('f3');
      // }
  
      // if(this.formSeat.value.seat4a == true && $('#a4').prop('disabled') == false){
      //   this.choosenSeat.push('a4');
      // }
      // if(this.formSeat.value.seat4b == true && $('#b4').prop('disabled') == false){
      //   this.choosenSeat.push('b4');
      // }
      // if(this.formSeat.value.seat4c == true && $('#c4').prop('disabled') == false){
      //   this.choosenSeat.push('c4');
      // }
      // if(this.formSeat.value.seat4d == true && $('#d4').prop('disabled') == false){
      //   this.choosenSeat.push('d4');
      // }
      // if(this.formSeat.value.seat4e == true && $('#e4').prop('disabled') == false){
      //   this.choosenSeat.push('e4');
      // }
      // if(this.formSeat.value.seat4f == true && $('#f4').prop('disabled') == false){
      //   this.choosenSeat.push('f4');
      // }
  
      // if(this.formSeat.value.seat5a == true && $('#a5').prop('disabled') == false){
      //   this.choosenSeat.push('a5');
      // }
      // if(this.formSeat.value.seat5b == true && $('#b5').prop('disabled') == false){
      //   this.choosenSeat.push('b5');
      // }
      // if(this.formSeat.value.seat5c == true && $('#c5').prop('disabled') == false){
      //   this.choosenSeat.push('c5');
      // }
      // if(this.formSeat.value.seat5d == true && $('#d5').prop('disabled') == false){
      //   this.choosenSeat.push('d5');
      // }
      // if(this.formSeat.value.seat5e == true && $('#e5').prop('disabled') == false){
      //   this.choosenSeat.push('e5');
      // }
      // if(this.formSeat.value.seat5f == true && $('#f5').prop('disabled') == false){
      //   this.choosenSeat.push('f5');
      // }
  
      // if(this.formSeat.value.seat6a == true && $('#a6').prop('disabled') == false){
      //   this.choosenSeat.push('a6');
      // }
      // if(this.formSeat.value.seat6b == true && $('#b6').prop('disabled') == false){
      //   this.choosenSeat.push('b6');
      // }
      // if(this.formSeat.value.seat6c == true && $('#c6').prop('disabled') == false){
      //   this.choosenSeat.push('c6');
      // }
      // if(this.formSeat.value.seat6d == true && $('#d6').prop('disabled') == false){
      //   this.choosenSeat.push('d6');
      // }
      // if(this.formSeat.value.seat6e == true && $('#e6').prop('disabled') == false){
      //   this.choosenSeat.push('e6');
      // }
      // if(this.formSeat.value.seat6f == true && $('#f6').prop('disabled') == false){
      //   this.choosenSeat.push('f6');
      // }
  
      // if(this.formSeat.value.seat7a == true && $('#a7').prop('disabled') == false){
      //   this.choosenSeat.push('a7');
      // }
      // if(this.formSeat.value.seat7b == true && $('#b7').prop('disabled') == false){
      //   this.choosenSeat.push('b7');
      // }
      // if(this.formSeat.value.seat7c == true && $('#c7').prop('disabled') == false){
      //   this.choosenSeat.push('c7');
      // }
      // if(this.formSeat.value.seat7d == true && $('#d7').prop('disabled') == false){
      //   this.choosenSeat.push('d7');
      // }
      // if(this.formSeat.value.seat7e == true && $('#e7').prop('disabled') == false){
      //   this.choosenSeat.push('e7');
      // }
      // if(this.formSeat.value.seat7f == true && $('#f7').prop('disabled') == false){
      //   this.choosenSeat.push('f7');
      // }
  
      // if(this.formSeat.value.seat8a == true && $('#a8').prop('disabled') == false){
      //   this.choosenSeat.push('a8');
      // }
      // if(this.formSeat.value.seat8b == true && $('#b8').prop('disabled') == false){
      //   this.choosenSeat.push('b8');
      // }
      // if(this.formSeat.value.seat8c == true && $('#c8').prop('disabled') == false){
      //   this.choosenSeat.push('c8');
      // }
      // if(this.formSeat.value.seat8d == true && $('#d8').prop('disabled') == false){
      //   this.choosenSeat.push('d8');
      // }
      // if(this.formSeat.value.seat8e == true && $('#e8').prop('disabled') == false){
      //   this.choosenSeat.push('e8');
      // }
      // if(this.formSeat.value.seat8f == true && $('#f8').prop('disabled') == false){
      //   this.choosenSeat.push('f8');
      // }

      // this.flagChoose = 1;
    // }
    console.log(this.choosenSeat);
    // this.sub.unsubscribe();
    if(this.choosenSeat.length == 0){
      this.showError();
    }else{
      this.resultParam['movieid'] = this.movieId;
      this.resultParam['date'] = this.scheduleDate;
      this.resultParam['time'] = this.scheduleTime;
      this.resultParam['studioid'] = this.studioId;
      this.resultParam['seat'] = this.choosenSeat;
      this.resultParam['harga'] = 0;
      if(this.hari == 5){
        this.resultParam['harga'] = 50000;
      }else if(this.hari == 6 || this.hari == 0 ){
        this.resultParam['harga'] = 60000;
      }else{
        this.resultParam['harga'] = 35000;
      }
      console.log(this.resultParam);
      this.navCtrl.push(PaymentPage,{param1:this.resultParam,paramFilm: this.film});
    }
  }

  showError(){
    const alert = this.alertCtrl.create({
      title: 'Information',
      message: 'Please choose seat',
      buttons: [
        {
          text: 'OK',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }

  initializeForm(){
    this.formSeat = new FormGroup({
      seat1a: new FormControl(null),
      seat1b: new FormControl(null),
      seat1c: new FormControl(null),
      seat1d: new FormControl(null),
      seat1e: new FormControl(null),
      seat1f: new FormControl(null),
      seat1g: new FormControl(null),
      seat1h: new FormControl(null),

      seat2a: new FormControl(null),
      seat2b: new FormControl(null),
      seat2c: new FormControl(null),
      seat2d: new FormControl(null),
      seat2e: new FormControl(null),
      seat2f: new FormControl(null),
      seat2g: new FormControl(null),
      seat2h: new FormControl(null),

      seat3a: new FormControl(null),
      seat3b: new FormControl(null),
      seat3c: new FormControl(null),
      seat3d: new FormControl(null),
      seat3e: new FormControl(null),
      seat3f: new FormControl(null),
      seat3g: new FormControl(null),
      seat3h: new FormControl(null),

      seat4a: new FormControl(null),
      seat4b: new FormControl(null),
      seat4c: new FormControl(null),
      seat4d: new FormControl(null),
      seat4e: new FormControl(null),
      seat4f: new FormControl(null),
      seat4g: new FormControl(null),
      seat4h: new FormControl(null),

      seat5a: new FormControl(null),
      seat5b: new FormControl(null),
      seat5c: new FormControl(null),
      seat5d: new FormControl(null),
      seat5e: new FormControl(null),
      seat5f: new FormControl(null),
      seat5g: new FormControl(null),
      seat5h: new FormControl(null),

      seat6a: new FormControl(null),
      seat6b: new FormControl(null),
      seat6c: new FormControl(null),
      seat6d: new FormControl(null),
      seat6e: new FormControl(null),
      seat6f: new FormControl(null),
      seat6g: new FormControl(null),
      seat6h: new FormControl(null),

      seat7a: new FormControl(null),
      seat7b: new FormControl(null),
      seat7c: new FormControl(null),
      seat7d: new FormControl(null),
      seat7e: new FormControl(null),
      seat7f: new FormControl(null),
      seat7g: new FormControl(null),
      seat7h: new FormControl(null),

      seat8a: new FormControl(null),
      seat8b: new FormControl(null),
      seat8c: new FormControl(null),
      seat8d: new FormControl(null),
      seat8e: new FormControl(null),
      seat8f: new FormControl(null),
      seat8g: new FormControl(null),
      seat8h: new FormControl(null)
    });
  }

}

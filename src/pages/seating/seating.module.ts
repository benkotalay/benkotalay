import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeatingPage } from './seating';

@NgModule({
  declarations: [
    SeatingPage,
  ],
  imports: [
    IonicPageModule.forChild(SeatingPage),
  ],
})
export class SeatingPageModule {}

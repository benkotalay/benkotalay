import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import {SeatingPage} from '../seating/seating';

/**
 * Generated class for the PopovermenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  templateUrl: 'popovermenu.html',
})
export class PopovermenuPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopovermenuPage');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  chooseSeat(size:any){
    this.navCtrl.push(SeatingPage);
  }

}

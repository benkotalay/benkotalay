import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { SeatingPage } from '../seating/seating';
import firebase from 'firebase';
import { AuthService } from '../../services/authService';
import { LoginPage } from '../login/login';
import { SynopsisPage } from '../synopsis/synopsis';
import { TrailerPage } from '../trailer/trailer';
// import { Movie } from '../../interface/movie-interface';
// import { Observable, Subscription } from 'rxjs/Rx';
// import { MovieProvider } from '../../providers/movie-provider';

/**
 * Generated class for the MovieDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html',
})
export class MovieDetailPage implements OnInit{
  formSchedule: FormGroup;
  movie: any;
  // movie: Movie;
  // private movieSub: Subscription;
  today: any;
  tomorrow: any;
  flagPage: any;
  date: any;
  newDate: any;
  flagUser: any;
  // constructor(public navCtrl: NavController, public navParams: NavParams, private movieProvider:MovieProvider) {
  constructor(public modalController: ModalController, public alertController: AlertController, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
    this.movie = this.navParams.get("movie");
    console.log(this.movie);
    this.flagPage = this.navParams.get("flagPage");
    this.flagUser = this.authService.flagUser;
    // const id = this.navParams.get("movie");
    // console.log(id.movie_id);
    // this.movieSub = this.movieProvider.getMovieDetails(id.movie_id)
    //   .subscribe(movie => this.movie = movie);
    console.log(this.movie);
    this.setTodayAndTomorrow();
  }

  ngOnInit(){
    this.initializeForm();
  }

  setTodayAndTomorrow(){
    var date = new Date();
    this.date = date;
    var year = date.getFullYear().toString();
    var bulan = date.getMonth()+1;
    // console.log(date.getDay());
    var month;
    if(bulan < 10){
      month = '0'+bulan;
    }else{
      month = bulan;
    }
    var tanggal = date.getDate();
    var day;
    if(tanggal < 10){
      day = '0'+tanggal;
    }else{
      day = tanggal;
    }
    this.today = day+"-"+month+"-"+year;

    var newDate = new Date(date);
    newDate.setDate(newDate.getDate()+1);
    this.newDate = newDate;
    year = newDate.getFullYear().toString();
    bulan = newDate.getMonth()+1;
    // console.log(newDate.getDay());
    if(bulan < 10){
      month = '0'+bulan;
    }else{
      month = bulan;
    }
    tanggal = newDate.getDate();
    if(tanggal < 10){
      day = '0'+tanggal;
    }else{
      day = tanggal;
    }
    this.tomorrow = day+"-"+month+"-"+year;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovieDetailPage');
  }

  onSubmit(){
    // console.log(this.formSchedule.value);
    var hari;
    if(this.today == this.formSchedule.value.date){
      hari = this.date.getDay();
    }else{
      hari = this.newDate.getDay();
    }
    var date = this.formSchedule.value.date.split('-');
    date = date[2]+"-"+date[1]+"-"+date[0];
    console.log(date+" "+this.formSchedule.value.time+" "+this.movie.movie_id);
    this.navCtrl.push(SeatingPage,{paramDate:date,paramTime:this.formSchedule.value.time,paramMovie:this.movie.movie_id,paramStudio:this.movie.studio_id,paramHari: hari, paramFilm: this.movie});
  }

  initializeForm(){
    this.formSchedule = new FormGroup({
      date: new FormControl(null,Validators.required),
      time: new FormControl(null,Validators.required)
    });
  }

  goToSignIn(){
    this.navCtrl.setRoot(LoginPage);
  }

  clickSynopsis(){
    // let synopModal = this.navCtrl.push(SynopsisPage, {movie:this.movie});
    this.navCtrl.push(SynopsisPage, {movie:this.movie});
  }

  clickTrailer(){
    this.navCtrl.push(TrailerPage,{movie: this.movie});
  }
}

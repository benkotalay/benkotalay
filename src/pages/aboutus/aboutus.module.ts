import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AboutusPage } from './aboutus';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AboutusPage,
  ],
  imports: [
    IonicPageModule.forChild(AboutusPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCAIF0s2OQo9AqCefiTJEIFkA-0QRFe4hs'
    })
  ],
})
export class AboutusPageModule {}

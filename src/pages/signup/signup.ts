import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/authService';
import firebase from "firebase";
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private authService: AuthService, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  close(){
    this.viewCtrl.dismiss();
  }
  registerNewUser(form: NgForm){
    var fullname = form.value.fullname;
    var email = form.value.email;
    var password = form.value.password;
    var repassword = form.value.repassword;
    var phone = form.value.phone;
    if(password === repassword){
      this.authService.signup(email,password).then(user => {
        var line = 0;
        var selectDb = firebase.database().ref('users').once('value', snapshot =>{
          if(snapshot.val() != null){
            console.log(snapshot.val());
            line = snapshot.val().length;
          } else {
            line = 0;
          }
          var database = firebase.database().ref('users/'+firebase.auth().currentUser.uid).set({
            fullName: fullname,
            phone: phone,
            saldo: 0
          });
        });
        //this.navCtrl.setRoot(SignupPage);
        console.log(form.value);
        // this.viewCtrl.dismiss();
      }).catch(error => {
        const alert = this.alertCtrl.create({
          title: 'Error',
          message: error.message,
          buttons: [{
            text: 'Ok',
            role: 'cancel',
          }
          ]
        });
        alert.present();
      }
      );
    }
    else{
      const alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Your password and re-type password doesn\'t match ',
        buttons: [{
          text: 'Ok',
          role: 'cancel',
        }
        ]
      });
      alert.present();
    }
  }
  signIn(){
    this.navCtrl.pop();
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HistoryPage } from '../history/history';

/**
 * Generated class for the SideHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-side-history',
  templateUrl: 'side-history.html',
})
export class SideHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SideHistoryPage');
  }

  goToShowHistoryTicket(){
    this.navCtrl.push(HistoryPage,{paramPage: "ticket"});
  }

  goToShowHistoryVoucher(){
    this.navCtrl.push(HistoryPage,{paramPage: "voucher"});
  }

}

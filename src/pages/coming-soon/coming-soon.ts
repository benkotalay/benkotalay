import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MovieDetailPage } from '../movie-detail/movie-detail';
import { MovieService } from '../../services/movieService';

/**
 * Generated class for the ComingSoonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-coming-soon',
  templateUrl: 'coming-soon.html',
})
export class ComingSoonPage {
  soonMovies: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public movieService: MovieService) {
    this.movieService.getComingSoon().then((res) => {
      console.log(res);
      this.soonMovies = res;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComingSoonPage');
  }

  showMovieDetail(idx: any){
    this.navCtrl.push(MovieDetailPage,{movie:this.soonMovies[idx],flagPage:0});
  }

}

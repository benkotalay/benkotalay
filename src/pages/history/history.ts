import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseService } from '../../services/databaseService';
import firebase from 'firebase';
import { MovieService } from '../../services/movieService';
import { TicketPage } from '../ticket/ticket';

/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  item: any;
  data: any;
  movie: any = [];
  title: any = [];
  poster: any = [];
  dateBuy: any = [];


  voucher: any = [];
  expiredDate: any = [];
  amount: any = [];
  used: any = [];
  vid: any = [];

  dateNow: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public dbSvc: DatabaseService, public movieSerice: MovieService) {
    var date = new Date();
    var year = date.getFullYear().toString();
    var bulan = date.getMonth()+1;
    var month;
    if(bulan < 10){
      month = '0'+bulan;
    }else{
      month = bulan;
    }
    var tanggal = date.getDate();
    var day;
    if(tanggal < 10){
      day = '0'+tanggal;
    }else{
      day = tanggal;
    }
    this.dateNow = year+"-"+month+"-"+day;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  ionViewWillEnter(){
    this.item = this.navParams.get("paramPage");
    if(this.item == "ticket"){
      this.dbSvc.historyData(this.item,firebase.auth().currentUser.uid).then((res) => {
        this.data = res;
        var length = this.data.length;
        console.log(res);
        for(let i=0; i<length;i++){
          this.movieSerice.getDetails(res[i]['ticket'][0]['movie_id']).then((result) => {
            console.log(result);
            this.movie[i] = result;
            this.title[i] = result['title'];
            this.poster[i] = result['poster'];
            var date = res[i]['ticket'][0]['sold_time'].split(" ");
            date = date[0].split("-");
            this.dateBuy[i] = date[2]+"-"+date[1]+"-"+date[0];
          })
        }
      })
    }else if(this.item == "voucher"){
      this.dbSvc.historyData(this.item,firebase.auth().currentUser.uid).then((res) => {
        this.data = res;
        var length = this.data.length;
        console.log(res);
        for(let i=0; i<length;i++){
          this.voucher[i] = res[i];
          var date = res[i]['expire_date'].split("-");
          this.expiredDate[i] = date[2]+"-"+date[1]+"-"+date[0];
          this.amount[i] = res[i]['amount'];
          this.vid[i] = res[i]['vid'];
          this.used[i] = "";
          if(res[i]['used'] == 1){
            this.used[i] = "Not used";
          }else if(res[i]['expire_date'] < this.dateNow){
            this.used[i] = "Expired";
          }else if(res[i]['used'] == 0){
            this.used[i] = "Used";
          }
        }
      })
    }
  }

  goToTicketPage(data: any){
    console.log(data);
    if(this.item == "ticket"){
      data['date'] = data['ticket'][0]['watching_date'];
      data['time'] = data['ticket'][0]['show_number'];
      data['studioid'] = data['ticket'][0]['seat_id'].substring(0,7);
      data['seat'] = [];
      for(let i = 0; i<data['ticket'].length;i++){
        data['seat'].push(data['ticket'][i]['seat_id'].substring(7));
      }
      data['harga'] = data['ticket'][0]['price'];
      data['movieid'] = data['ticket'][0]['movie_id'];
      data['orderid'] = data['ticket'][0]['order_id'];
      this.navCtrl.push(TicketPage,{param1: data});
    }else if(this.item == "voucher"){

    }
    
  }

}

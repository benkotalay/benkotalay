import { ShowMovie } from './../../data/showMovie.interface';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { DatabaseService } from '../../services/databaseService';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { PopovermenuPage} from '../popovermenu/popovermenu'
import { MovieService } from '../../services/movieService';
import { MovieDetailPage } from '../movie-detail/movie-detail';
/**
 * Generated class for the PlayingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playing',
  templateUrl: 'playing.html',
})
export class PlayingPage implements OnInit{
  private showMovies: any;
  today: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public dbSvc: DatabaseService, public popoverCtrl: PopoverController, public movieService: MovieService) {
    this.setToday();
    this.dbSvc.getShowMovies().then((res) => {
      this.showMovies = res;
      var arr = [];
      for(let i = 0; i<this.showMovies.length; i++){
        if(this.today>=this.showMovies[i].startda && this.today<=this.showMovies[i].endda){
          arr.push(this.showMovies[i]);
        }
      }
      this.showMovies = arr;
      this.movieService.getAllShowMovies(this.showMovies).then((result) => {
        this.showMovies = result;
      });
    });
  }
  
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad PlayingPage');
  }
  ionViewWillEnter(){
    
  }
  ngOnInit(){
    //console.log(this.dbSvc.requestData("http://localhost/phpShowMovies.php"));
    // this.showMovies = this.movieService.getShowMovies();
    // console.log(this.showMovies);
    
    // this.showMovies = this.dbSvc.getShowMovies();
    // this.showMovies = this.movieService.getAllShowMovies(this.showMovies);
  }

  setToday(){
    var date = new Date();
    var year = date.getFullYear().toString();
    var bulan = date.getMonth()+1;
    var month;
    if(bulan < 10){
      month = '0'+bulan;
    }else{
      month = bulan;
    }
    var tanggal = date.getDate();
    var day;
    if(tanggal < 10){
      day = '0'+tanggal;
    }else{
      day = tanggal;
    }
    this.today = year+"-"+month+"-"+day;
  }

  getShowMovies(){
    this.showMovies = this.movieService.getShowMovies();
    console.log(this.showMovies);
  }

  presentPopover(myEvent) {
    //(click)="presentPopover($event)" -> popover yang digunakan ketika movie di click
    let popover = this.popoverCtrl.create(PopovermenuPage);
    popover.present({
      ev: myEvent
    });
  }

  showMovieDetail(idx: any){
    this.navCtrl.push(MovieDetailPage,{movie:this.showMovies[idx],flagPage:1});
  }
}

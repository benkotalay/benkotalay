import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import { TabsPage } from '../tabs/tabs';
import { MovieService } from '../../services/movieService';

/**
 * Generated class for the TicketPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ticket',
  templateUrl: 'ticket.html',
})
export class TicketPage {
  detail: any;
  film: any;
  saldo: any;
  time: any;
  studio: any;
  date: any;
  totalPrice: any;
  title: any;
  orderid: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public movieService: MovieService) {
  this.detail = this.navParams.get('param1');
    this.time = this.detail.time;
    if(this.time == "1"){
      this.time = "12:00";
    }else if(this.time == "2"){
      this.time = "15:00";
    }else if(this.time == "3"){
      this.time = "18:00";
    }else if(this.time == "4"){
      this.time = "21:00";
    }
    this.studio = this.detail.studioid.substring(5);
    this.date = this.detail.date.split("-");
    this.date = this.date[2]+"-"+this.date[1]+"-"+this.date[0];
    this.totalPrice = this.detail.harga*this.detail.seat.length;
    this.orderid = this.detail.orderid;
    this.film = this.navParams.get('paramFilm');

    console.log(this.film);
    // console.log(this.detail);
    this.movieService.getDetails(this.detail.movieid).then((res) => {
      this.title = res['title'];
    })
    firebase.database().ref('users/' + firebase.auth().currentUser.uid).once('value', snapshot => {
      this.saldo = snapshot.val().saldo;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TicketPage');
  }

  goToHome(){
    this.navCtrl.setRoot(TabsPage);
  }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { MovieService } from '../../services/movieService';
import { MovieDetailPage } from '../movie-detail/movie-detail';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage implements OnInit{
  title: any = "";
  searchMovies: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public movieService: MovieService, public loadingCtrl: LoadingController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  ngOnInit(){

  }

  showMovieDetail(idx: any){
    this.navCtrl.push(MovieDetailPage,{movie:this.searchMovies[idx],flagPage:0});
  }

  searchMovie(judul: string){
    // console.log(judul);
    if(judul != null || judul == ""){
      this.title = judul;
      this.movieService.getDataSearch(this.title).then((res) => {
        // console.log(res);
        let loading = this.loadingCtrl.create({
          spinner: 'bubbles',
          content: 'Loading Please Wait...'
        });
      
        loading.present();
      
        setTimeout(() => {
          this.searchMovies = res;
          loading.dismiss();
        }, 2000);
      });
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import firebase from "firebase";
import { HistoryPage } from '../history/history';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit{
  changePasswordForm: FormGroup;
  fullName: any;
  email: any;
  phone: any;
  saldo: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  ngOnInit(){
    this.initializeForm();
    
    firebase.database().ref('users/'+firebase.auth().currentUser.uid).once('value',snapshot => {
      this.email = firebase.auth().currentUser.email;
      this.fullName = snapshot.val().fullName;
      this.phone = snapshot.val().phone;
      this.saldo = snapshot.val().saldo;
      if(this.saldo == 0){
        this.saldo = "0";
      }
    })
  }

  initializeForm(){
    this.changePasswordForm = new FormGroup({
      newPass: new FormControl(null,Validators.required),
      reTypePass: new FormControl(null,Validators.required)
    })
  }

  changePass(form: NgForm){
    var newPass = form.value.newPass;
    var reTypePass = form.value.reTypePass;
    let alertConf = this.alertCtrl.create({
      title: 'Confirmation',
      subTitle: 'Are you sure to change your password?',
      buttons:[
        { 
          text: 'Yes',
          handler: res=>{
            if(newPass == reTypePass){
              
              var user = firebase.auth().currentUser;
              console.log(user);
              user.updatePassword(newPass).then(function(){
                //update successfull
                console.log('update successfull');
                let alert = this.alertCtrl.create({
                  title: 'Success',
                  message: 'Your password has been changed',
                  buttons: [{
                    text: 'Ok',
                    role: 'cancel',
                  }
                  ]
                });
                alert.present();
              }).catch(function(err){
                //error update
                console.log(err);
              });
            }else{
              let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'Your password and re-type password doesn\'t match ',
                buttons: [{
                  text: 'Ok',
                  role: 'cancel',
                }
                ]
              });
              alert.present();
            }
          }
        },
        {
          text: "No",
          role: 'cancel'
        }
      ]
    });
    alertConf.present();
    
  }

  goToHistoryPurchase(item: any){
    this.navCtrl.push(HistoryPage,{paramPage: item});
  }

}

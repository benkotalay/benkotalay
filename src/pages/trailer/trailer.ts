import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import YouTubePlayer from 'youtube-player';

/**
 * Generated class for the TrailerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trailer',
  templateUrl: 'trailer.html',
})
export class TrailerPage {
  movie: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.movie = this.navParams.get("movie");
    console.log(this.movie.youtube);

    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrailerPage');

    let player;
 
    player = YouTubePlayer("video-player");
    
    // 'loadVideoById' is queued until the player is ready to receive API calls. 
    player.loadVideoById(this.movie.youtube);
    
    // 'playVideo' is queue until the player is ready to received API calls and after 'loadVideoById' has been called. 
    player.playVideo();
    
    // 'stopVideo' is queued after 'playVideo'. 
    player
        .stopVideo()
        .then(() => {
            // Every function returns a promise that is resolved after the target function has been executed. 
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import { TopUpPage } from '../top-up/top-up';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { DatabaseService } from '../../services/databaseService';
import { TicketPage } from '../ticket/ticket';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  detail: any;
  film: any;
  saldo: any;
  time: any;
  studio: any;
  date: any;
  totalPrice: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public dbSvc: DatabaseService) {
    this.detail = this.navParams.get('param1');
    this.time = this.detail.time;
    if(this.time == "1"){
      this.time = "12:00";
    }else if(this.time == "2"){
      this.time = "15:00";
    }else if(this.time == "3"){
      this.time = "18:00";
    }else if(this.time == "4"){
      this.time = "21:00";
    }
    this.studio = this.detail.studioid.substring(5);
    this.date = this.detail.date.split("-");
    this.date = this.date[2]+"-"+this.date[1]+"-"+this.date[0];
    this.totalPrice = this.detail.harga*this.detail.seat.length;
    this.film = this.navParams.get('paramFilm');
    console.log(this.detail);
    firebase.database().ref('users/' + firebase.auth().currentUser.uid).once('value', snapshot => {
      this.saldo = snapshot.val().saldo;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }

  goToTopUp(){
    this.navCtrl.push(TopUpPage);
  }

  ionViewWillEnter(){
    firebase.database().ref('users/' + firebase.auth().currentUser.uid).once('value', snapshot => {
      this.saldo = snapshot.val().saldo;
    });
  }

  goToPay(){
    console.log("test");
    if(this.saldo < this.totalPrice){
      const alert = this.alertCtrl.create({
        title: 'Information',
        message: 'Insufficient fund. Please top up to continue',
        buttons: [
          {
            text: 'OK',
            role: 'cancel'
          }
        ]
      });
      alert.present();
    }else{
      const alert = this.alertCtrl.create({
        title: 'Information',
        message: 'Are you sure you want to buy?',
        buttons: [
          {
            text: 'OK',
            handler: () => {
              this.dbSvc.getLastSeatToPay(this.detail.movieid,this.detail.date,this.detail.time,this.detail.studioid).then((res) => {
                var flagLoop = 1;
                if(res['success']==1){
                  console.log("test4");
                  for(let i = 0;i<res['buytickets'].length;i++){
                    var id = res['buytickets'][i]['seat_id'];
                    id = id.substring(7);
                    // console.log(id);
                    console.log("test1");
                    for(let i = 0; i<this.detail.seat.length;i++){
                      if(id == this.detail.seat[i]){
                        flagLoop = 0;
                        break;
                      }
                    }
                    if(flagLoop == 0){
                      break;
                    }
                  }
                }
                console.log(flagLoop);
                if(flagLoop == 1){
                  firebase.database().ref('users/' + firebase.auth().currentUser.uid).update({
                    saldo: this.saldo-this.totalPrice
                  });
                  var length = this.detail.seat.length;
                  var stringSeat = "";
                  for(let i = 0; i<length;i++){
                    var seatId = this.detail.studioid+this.detail.seat[i];
                    console.log(seatId);
                    if(i == 0){
                      stringSeat = seatId;
                    }else{
                      stringSeat = stringSeat + "-" + seatId;
                    }
                    
                  }
                  console.log(stringSeat);
                  this.dbSvc.insertBuyTickets(this.detail.movieid,this.detail.date,this.detail.time,stringSeat,this.detail.harga,firebase.auth().currentUser.uid).then((res) => {
                    console.log(res);
                    this.navCtrl.setRoot(TicketPage,{param1: this.detail,paramFilm: this.film});
                 });
                }else{
                  const alert = this.alertCtrl.create({
                    title: 'Information',
                    message: 'Your choosen seat has already taken by someone else, please choose another seat.',
                    buttons: [
                      {
                        text: 'OK',
                        role: 'cancel'
                      }
                    ]
                  });
                  alert.present();
                  this.navCtrl.pop();
                }
              });
            }
          },
          {
            text: 'Cancel',
            role: 'cancel'
          }
        ]
      });
      alert.present();
    }
  }
}

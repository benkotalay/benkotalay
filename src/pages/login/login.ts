import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { AuthService } from '../../services/authService';
import { NgForm } from '@angular/forms';
import firebase from 'firebase';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private modalCtrl: ModalController, private authService: AuthService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  ngOnInit(){

  }

  onSubmit(form: NgForm){
    // console.log(form.value);
    this.authService.signin(form.value.email, form.value.password).then(param=>{
      this.navCtrl.setRoot(TabsPage);
    }).catch(
      error => {
        const alert = this.alertCtrl.create({
          title: 'Error',
          message: error.message,
          buttons: [{
            text: 'Ok',
            role: 'cancel',
          }
          ]
        });
        alert.present();
      }
    );
  }

  forgotPass(){
    const resetPass = this.alertCtrl.create({
      title: 'Reset Password',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Reset',
          handler: data => {
            var email = data.email;
            var auth = firebase.auth();
            const alertSuccess = this.alertCtrl.create({
              title: 'Information',
              message: "Please check your email to get new password.",
              buttons: [{
                text: 'Ok',
                role: 'cancel',
              }
              ]
            });
            const alertError = this.alertCtrl.create({
              title: 'Error',
              message: "There's something error in this function. Check your email address or contact admin.",
              buttons: [{
                text: 'Ok',
                role: 'cancel',
              }
              ]
            });
            auth.sendPasswordResetEmail(email).then(function(){
              alertSuccess.present();
            }).catch(function(err){
              alertError.present();
            });
          }
        }
      ]
    });
    resetPass.present();
  }

  goToRegister(){
    this.navCtrl.push(SignupPage);
  }

  loginPrompt(){
    const signIn = this.alertCtrl.create({
      title: 'Sign In',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Password'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Sign In',
          handler: data=> {
            var email = data.email;
            var password = data.password;
            this.authService.signin(email, password).then().catch(
              error => {
                const alert = this.alertCtrl.create({
                  title: 'Error',
                  message: error.message,
                  buttons: [{
                    text: 'Ok',
                    role: 'cancel',
                  }
                  ]
                });
                alert.present();
              }
            );
          }
        }
      ]
    });
    signIn.present();
  }
  signOut(){
    this.authService.logout();
  }
  // registerPrompt(){
  //   const signUp = this.alertCtrl.create({
  //     title: 'Sign Up',
  //     inputs: [
  //       {
  //         name: 'fullname',
  //         placeholder: 'Full Name'
  //       },
  //       {
  //         name: 'username',
  //         placeholder: 'Username'
  //       },
  //       {
  //         name: 'password',
  //         type: 'password',
  //         placeholder: 'Password'
  //       },
  //       {
  //         name: 're-password',
  //         type: 'password',
  //         placeholder: 'Re-enter your Password'
  //       },
  //       {
  //         name: 'email',
  //         type: 'email',
  //         placeholder: 'Email'
  //       },
  //       {
  //         name: 'phone',
  //         type: 'number',
  //         placeholder: 'Phone Number'
  //       },
  //     ],
  //     buttons:[
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         handler: data=> {
  //           console.log('Cancel clicked')
  //         }
  //       },
  //       {
  //         text: 'Sign Up',
  //         handler: data=> {
            
  //         }
  //       }
  //     ]
  //   });
  //   signUp.present();
  // }
  register(){
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
  }

}

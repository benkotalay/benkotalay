import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Title } from '@angular/platform-browser/src/browser/title';
import { AuthService } from '../../services/authService';
import firebase from 'firebase';
import { DatabaseService } from '../../services/databaseService';
// import { EmailComposer } from '@ionic-native/email-composer';
import { Http } from '@angular/http';
import { Button } from 'ionic-angular/components/button/button';
import YouTubePlayer from 'youtube-player';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

/**
 * Generated class for the TopUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-top-up',
  templateUrl: 'top-up.html',
})
export class TopUpPage implements OnInit{
  //formGetSaldo: FormGroup;
  formBuyVoucher: FormGroup;
  balance: string;
  // constructor(public http: Http, private emailComposer: EmailComposer, public dbService: DatabaseService, public authService:AuthService, public alertController: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  constructor(public http: Http, public dbService: DatabaseService, public authService:AuthService, public alertController: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TopUpPage');
  }

  ngOnInit(){
    this.initializeForm();
    console.log(firebase.auth().currentUser.uid);
    firebase.database().ref('users/' + firebase.auth().currentUser.uid).once('value',snapshot=>{
      this.balance = snapshot.val().saldo;
      console.log(snapshot.val().saldo);
    });
  }

  initializeForm(){
    // this.formGetSaldo = new FormGroup({
    //   codeVoucher: new FormControl(null,Validators.required)
    // });
    this.formBuyVoucher = new FormGroup({
      amountVoucher: new FormControl(null,Validators.required),
      ccNumber: new FormControl(null,Validators.required),
      ccv: new FormControl(null,Validators.required)
    });
  }

  getSaldo(){
    let alertRedeem = this.alertController.create({
      title: 'Redeem Balance Voucher',
      subTitle: 'Please insert your paid voucher before!',
      inputs: [
        {
          name: 'voucherCode',
          placeholder: 'Voucher Code',
          type: 'text'
        }
      ],
      buttons:[
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Submit',
          handler: data=> {
            var curUser = firebase.auth().currentUser.uid;
            this.dbService.redeemVoucher(data.voucherCode).then(res => {
              console.log(res);
              if(res != "fail"){
                console.log(parseInt(res['amount']) + parseInt(this.balance));
                firebase.database().ref('users/' + firebase.auth().currentUser.uid).update({
                  saldo: parseInt(res['amount']) + parseInt(this.balance)
                });
                this.balance = (parseInt(res['amount']) + parseInt(this.balance)) + "";
                let alertConf = this.alertController.create({
                  title: 'Payment Confirmation',
                  subTitle: 'Your voucher is redeemed successfully',
                  buttons: [
                    {text: 'Ok'}
                  ]
                })
                alertConf.present();
              } else {
                let alertConf = this.alertController.create({
                  title: 'Payment Confirmation',
                  subTitle: 'Voucher not found or expired!',
                  buttons: [
                    {text: 'Ok'}
                  ]
                })
                alertConf.present();
              }
              
            });
          }
        }
      ]
    });
    alertRedeem.present();
  }

  buyVoucher(){
    console.log(this.formBuyVoucher.value);
    console.log(firebase.auth().currentUser);
    this.dbService.buyVoucher(this.formBuyVoucher.value.amountVoucher, this.formBuyVoucher.value.ccNumber, firebase.auth().currentUser.uid).then(res => {
      console.log(res);
      
      let voucherAlert = this.alertController.create({
        title: "Ticket",
        message: 'Your Voucher: ' + res,
        buttons: [
          {text: 'Ok',
          handler: () => {
            this.formBuyVoucher.value.amountVoucher = null;
            this.formBuyVoucher.value.ccNumber = null;
            this.formBuyVoucher.value.ccv = null;
            
          }},
          
        ],
        
      });
      voucherAlert.present();
      // this.http.get("")
      //       .map(res => res.json())
      //       .subscribe(data => {
      //       //   console.log(data.showMovies);
      //         this.showMovies = data.showMovies;
      //         resolve(data.showMovies);
      //       });

      
      // this.emailComposer.isAvailable().then((available: boolean) =>{
      //   if(available) {
      //     //Now we know we can send
      //     let email = {
      //       to: firebase.auth().currentUser.email,
      //       cc: '',
      //       bcc: ['bentengkotaalay@gmail.com'],
      //       attachments: [
              
      //       ],
      //       subject: 'Seenema Top Up Voucher',
      //       body: 'Your Voucher: ',
      //       isHtml: true
      //     };
          
      //     // Send a text message using default options
      //     this.emailComposer.open(email);
      //   }
      //  });
    });
  }
}
